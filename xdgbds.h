
#ifndef SRC_XDG_BASE_DIRS_H_
#define SRC_XDG_BASE_DIRS_H_

#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <sstream>
#include <vector>

// Implements the XDG Base Directory Specification to locate files. 

// Most of the following documentation is copied verbatim from the
// specification document.

namespace xdg {

namespace data {

// $XDG_DATA_HOME defines the base directory relative to which user specific
// data files should be stored. If $XDG_DATA_HOME is either not set or empty, a
// default equal to $HOME/.local/share should be used.

inline std::filesystem::path home()
{
        if (const char* s = std::getenv("XDG_DATA_HOME"))
                return s;
        if (const char* s = std::getenv("HOME"))
                return std::filesystem::path(s) / ".local/share";
        throw std::runtime_error{"Undefined HOME"};
}

// $XDG_DATA_DIRS defines the preference-ordered set of base directories to
// search for data files in addition to the $XDG_DATA_HOME base directory. The
// directories in $XDG_DATA_DIRS should be seperated with a colon ':'.

// If $XDG_DATA_DIRS is either not set or empty, a value equal to
// /usr/local/share/:/usr/share/ should be used.

inline std::vector<std::filesystem::path> dirs()
{
        std::vector<std::filesystem::path> ret;
        if (const char* s = std::getenv("XDG_DATA_DIRS")) {
                std::istringstream ss{s};
                std::string tok;
                while (!ss.eof()) {
                        std::getline(ss, tok, ':');
                        ret.push_back(tok);
                }
        } else {
                ret.push_back("/usr/local/share");
                ret.push_back("/usr/share");
        }
        return ret;
}

// Searches for a file with a given path in the defined home directories.
// If no such file exists, an exception is raised.

// The order of base directories denotes their importance; the first directory
// listed is the most important. When the same information is defined in
// multiple places the information defined relative to the more important base
// directory takes precedent. The base directory defined by $XDG_DATA_HOME is
// considered more important than any of the base directories defined by
// $XDG_DATA_DIRS.

inline std::filesystem::path find(const std::filesystem::path& path)
{
        try {
                std::filesystem::path f{home() / path};
                if (std::filesystem::exists(f))
                        return f;
        } catch (std::exception& ex) {
        }
        for (auto& d : dirs()) {
                std::filesystem::path f{d / path};
                if (std::filesystem::exists(f))
                        return f;
        }
        throw std::runtime_error{path.native() + ": no such file or directory"};
}

}; // namespace data

namespace config {

// $XDG_CONFIG_HOME defines the base directory relative to which user specific
// configuration files should be stored. If $XDG_CONFIG_HOME is either not set
// or empty, a default equal to $HOME/.config should be used.

inline std::filesystem::path home()
{
        if (const char* s = std::getenv("XDG_CONFIG_HOME"))
                return s;
        if (const char* s = std::getenv("HOME"))
                return std::filesystem::path(s) / ".config";
        throw std::runtime_error{"Undefined HOME"};
}

// $XDG_CONFIG_DIRS defines the preference-ordered set of base directories to
// search for configuration files in addition to the $XDG_CONFIG_HOME base
// directory. The directories in $XDG_CONFIG_DIRS should be seperated with a
// colon ':'.

// If $XDG_CONFIG_DIRS is either not set or empty, a value equal to /etc/xdg
// should be used.

inline std::vector<std::filesystem::path> dirs()
{
        std::vector<std::filesystem::path> ret;
        if (const char* s = std::getenv("XDG_CONFIG_DIRS")) {
                std::istringstream ss{s};
                std::string tok;
                while (!ss.eof()) {
                        std::getline(ss, tok, ':');
                        ret.push_back(tok);
                }
        } else {
                ret.push_back("/etc/xdg");
        }
        return ret;
}

// Searches for a file with a given path in the defined config directories.
// If no such file exists, an exception is raised.

// The order of base directories denotes their importance; the first directory
// listed is the most important. When the same information is defined in
// multiple places the information defined relative to the more important base
// directory takes precedent. The base directory defined by $XDG_CONFIG_HOME is
// considered more important than any of the base directories defined by
// $XDG_CONFIG_DIRS.

inline std::filesystem::path find(const std::filesystem::path& path)
{
        try {
                std::filesystem::path f{home() / path};
                if (std::filesystem::exists(f))
                        return f;
        } catch (std::exception& ex) {
        }
        for (auto& d : dirs()) {
                std::filesystem::path f{d / path};
                if (std::filesystem::exists(f))
                        return f;
        }
        throw std::runtime_error{path.native() + ": no such file or directory"};
}

}; // namespace config

namespace cache {

// $XDG_CACHE_HOME defines the base directory relative to which user specific
// non-essential data files should be stored. If $XDG_CACHE_HOME is either not
// set or empty, a default equal to $HOME/.cache should be used.

inline std::filesystem::path home()
{
        if (const char* s = std::getenv("XDG_CACHE_HOME"))
                return s;
        if (const char* s = std::getenv("HOME"))
                return std::filesystem::path(s) / ".cache";
        throw std::runtime_error{"Undefined HOME"};
}

// Searches for a file with a given path in the defined cache directory.
// If no such file exists, an exception is raised.

inline std::filesystem::path find(const std::filesystem::path& path)
{
        std::filesystem::path f{home() / path};
        if (std::filesystem::exists(f))
                return f;
        throw std::runtime_error{path.native() + ": no such file or directory"};
}

}; // namespace cache

namespace runtime {

// $XDG_RUNTIME_DIR defines the base directory relative to which user-specific
// non-essential runtime files and other file objects (such as sockets, named
// pipes, ...) should be stored.

inline std::filesystem::path dir()
{
        if (const char* s = std::getenv("XDG_RUNTIME_DIR"))
                return s;
        std::cerr << "Warning: undefined runtime dir, defaulting to /etc\n";
        return "/etc";
}

// Searches for a file with a given path in the defined runtime directory.
// If no such file exists, an exception is raised.

inline std::filesystem::path find(const std::filesystem::path& path)
{
        std::filesystem::path f{dir() / path};
        if (std::filesystem::exists(f))
                return f;
        throw std::runtime_error{path.native() + ": no such file or directory"};
}

}; // namespace runtime

}; // namespace xdg

#endif // SRC_XDG_BASE_DIRS_H_
