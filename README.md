XDG Base Directories for C++17
==============================

A simple, header-only library to locate files according to tho XDG Base
Directory Specification for C++17.

According to
https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

Usage
-----

Include the `xdgbds.h` file, use the contained functions, see the file
itself for details.

```cpp
std::filesystem::path xdg::data::home()
std::vector<std::filesystem::path> xdg::data::dirs()
std::filesystem::path xdg::data::find(const std::filesystem::path& path)
```

The `home()` and `dirs()` functions return the value of `XDG_DATA_HOME` and
`XDG_DATA_DIRS` respectively.  The `find()` function returns the full path of a
data file whose path relative to the base directories is given, or raises an
instance of `std::runtime_error` with an appropriate error message if no such
file exists.

```cpp
std::filesystem::path xdg::config::home()
std::vector<std::filesystem::path> xdg::config::dirs()
std::filesystem::path xdg::config::find(const std::filesystem::path& path)
```

The `home()` and `dirs()` functions return the value of `XDG_CONFIG_HOME` and
`XDG_CONFIG_DIRS` respectively.  The `find()` function returns the full path of
a config file whose path relative to the base directories is given, or raises
an instance of `std::runtime_error` with an appropriate error message if no
such file exists.

```cpp
std::filesystem::path xdg::cache::home()
std::filesystem::path xdg::cache::find(const std::filesystem::path& path)
```

The `home()` function returns the value of `XDG_CACHE_HOME`.  The `find()`
function returns the full path of a cache file whose path relative to the base
directories is given, or raises an instance of `std::runtime_error` with an
appropriate error message if no such file exists.

```cpp
std::filesystem::path xdg::runtime::dir()
std::filesystem::path xdg::runtime::find(const std::filesystem::path& path)
```

The `dir()` function returns the value of `XDG_RUNTIME_DIR`.  The `find()`
function returns the full path of a runtime file whose path relative to the
base directories is given, or raises an instance of `std::runtime_error` with
an appropriate error message if no such file exists.
